﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarAI_v2 {
	class WindowManager {

		public RenderWindow window;
		GameManager gm;

		public WindowManager () {
			window = new RenderWindow(new VideoMode(1000, 800), "Car AI");
			window.Closed += new EventHandler((object sender, EventArgs e) => {
				RenderWindow temp = (RenderWindow)sender;
				temp.Close();
			});

			gm = new GameManager(this);

			window.SetActive();
			window.SetFramerateLimit(60);

			while (window.IsOpen) {
				window.Clear(Color.White);
				gm.Update();
				window.DispatchEvents();
				window.Display();
			}
		}
	}
}
