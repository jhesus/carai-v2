﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarAI_v2 {
	class Car {
		Texture tex;
		Sprite sprite;
		GameManager gm;

		float turnAccel = 0.1f;
		float turnLimit = 0.5f;
		float turnDrag = 0.92f;
		float turnVelocity = 0;

		float acceleration = 0.45f;
		float speedLimit = 7f;
		float drag = 0.92f;
		Vector2f velocity = new Vector2f(0,0);

		public struct LineSegment {
			public Vertex p1;
			public Vertex p2;

			public LineSegment(Vertex point1, Vertex point2) : this() {
				p1 = point1;
				p2 = point2;
			}
		}

		public Car (GameManager gm) {
			this.gm = gm;
			tex = new Texture(@"sprites\car.png") {
				Smooth = false,
				Repeated = false,
			};

			sprite = new Sprite() {
				Texture = tex,
				Scale = new Vector2f(2, 2),
				Origin = new Vector2f(6.5f, 9.5f),
			};

			ResetCar();
		}

		public float VectorMagnitude (Vector2f v) {
			return (float)Math.Sqrt((v.X * v.X) + (v.Y * v.Y));
		}

		public Vector2f VectorLimit (Vector2f v, float limit) {
			var temp = v;

			if (temp.X < -limit) temp.X = -limit;
			if (temp.X > limit) temp.X = limit;
			if (temp.Y < -limit) temp.Y = -limit;
			if (temp.Y > limit) temp.Y = limit;

			return temp;
		}

		public float Clamp (float input, float limit) {
			float temp = input;
			if (temp < -limit) temp = -limit;
			if (temp > limit) temp = limit;
			return temp;
		}

		public void Update () {
			//rotation

			turnVelocity *= turnDrag;

			if (Keyboard.IsKeyPressed(Keyboard.Key.Left)) {
				//Console.WriteLine("left");
				//sprite.Transform.Rotate(-turnSpeed);
				turnVelocity -= Clamp (turnAccel * VectorMagnitude(velocity) * VectorMagnitude(velocity), turnLimit);
			}
			if (Keyboard.IsKeyPressed(Keyboard.Key.Right)) {
				turnVelocity += Clamp (turnAccel * VectorMagnitude(velocity) * VectorMagnitude(velocity), turnLimit);
			}

			sprite.Rotation += turnVelocity;


			//translation
			var direction = new Vector2f((float)Math.Cos((sprite.Rotation + 90) * (Math.PI/180)), (float)Math.Sin((sprite.Rotation + 90) * (Math.PI / 180)));
			velocity *= drag;

			if (Keyboard.IsKeyPressed(Keyboard.Key.Up)) {

				velocity -= direction * acceleration;
			}
			if (Keyboard.IsKeyPressed(Keyboard.Key.Down)) {
				velocity += direction * acceleration;
			}

			velocity = VectorLimit(velocity, speedLimit);

			sprite.Position = sprite.Position + velocity;

			// bounds
			var boundsList = new List<Vertex>() {
				//new Vertex (sprite.Position, Color.Green)
			};

			var topLeft = sprite.Transform.TransformPoint(0,0);
			var topRight = sprite.Transform.TransformPoint(sprite.Texture.Size.X, 0);
			var botRight = sprite.Transform.TransformPoint(sprite.Texture.Size.X, sprite.Texture.Size.Y);
			var botLeft = sprite.Transform.TransformPoint(0, sprite.Texture.Size.Y);
			boundsList.Add (new Vertex (topLeft, Color.Green));
			boundsList.Add(new Vertex(topRight, Color.Green));
			boundsList.Add(new Vertex(botRight, Color.Green));
			boundsList.Add(new Vertex(botLeft, Color.Green));

			var bounds = boundsList.ToArray();

			CheckCollisions(bounds);

			// drawing
			gm.wm.window.Draw(sprite);
			//gm.wm.window.Draw(bounds, PrimitiveType.LineStrip, RenderStates.Default);
		}

		public void CheckCollisions (Vertex[] bds) {
			for (int i = 0; i < bds.Count(); i++) {
				foreach (Vertex[] line in gm.allTrackLines) {
					if (i == bds.Count() - 1) {
						if (DoLinesIntersect(bds[i], bds[0], line[0], line[1])) {
							Die();
							return;
						}
					} else {
						if (DoLinesIntersect(bds[i], bds[i + 1], line[0], line[1])) {
							Die();
							return;
						}
					}
				}
			}
		}

		public void ResetCar () {
			sprite.Position = new Vector2f(500, 670);
			sprite.Rotation = 270;

			velocity = new Vector2f(0, 0);
			turnVelocity = 0;
		}

		public void Die () {
			//sprite.Color = Color.Red;

			ResetCar();
		}

		// maths 

		public bool DoLinesIntersect (Vertex a1_t, Vertex a2_t, Vertex b1_t, Vertex b2_t) {
			Vector2f v1 = a1_t.Position;
			Vector2f v2 = a2_t.Position;
			Vector2f v3 = b1_t.Position;
			Vector2f v4 = b2_t.Position;

			float uA = ((v4.X - v3.X) * (v1.Y - v3.Y) - (v4.Y - v3.Y) * (v1.X - v3.X)) / ((v4.Y - v3.Y) * (v2.X - v1.X) - (v4.X - v3.X) * (v2.Y - v1.Y));

			float uB = ((v2.X - v1.X) * (v1.Y - v3.Y) - (v2.Y - v1.Y) * (v1.X - v3.X)) / ((v4.Y - v3.Y) * (v2.X - v1.X) - (v4.X - v3.X) * (v2.Y - v1.Y));

			if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
				return true;
			}
			return false;
		}
	}
}
