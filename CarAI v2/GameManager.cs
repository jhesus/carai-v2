﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarAI_v2 {
	class GameManager {
		public WindowManager wm;
		public Car player;

		public List<Vertex> outerTrackVertices = new List<Vertex>() {
			new Vertex (new Vector2f(428, 764), Color.Black), new Vertex (new Vector2f(87, 751), Color.Black), new Vertex (new Vector2f(8, 569), Color.Black), new Vertex (new Vector2f(136, 354), Color.Black), new Vertex (new Vector2f(64, 156), Color.Black),
			new Vertex (new Vector2f(126, 26), Color.Black), new Vertex (new Vector2f(403, 53), Color.Black), new Vertex (new Vector2f(474, 190), Color.Black), new Vertex (new Vector2f(582, 214), Color.Black), new Vertex (new Vector2f(627, 190), Color.Black),
			new Vertex (new Vector2f(645, 107), Color.Black), new Vertex (new Vector2f(740, 59), Color.Black), new Vertex (new Vector2f(852, 76), Color.Black), new Vertex (new Vector2f(977, 405), Color.Black), new Vertex (new Vector2f(929, 558), Color.Black),
			new Vertex (new Vector2f(812, 669), Color.Black), new Vertex (new Vector2f(601, 683), Color.Black)
		};

		public List<Vertex[]> outerTrackLines;

		public List<Vertex> innerTrackVertices = new List<Vertex>() {
			new Vertex (new Vector2f(204, 640), Color.Black), new Vertex (new Vector2f(245, 335), Color.Black), new Vertex (new Vector2f(207, 160), Color.Black), new Vertex (new Vector2f(486, 272), Color.Black),
			new Vertex (new Vector2f(609, 282), Color.Black), new Vertex (new Vector2f(700, 239), Color.Black), new Vertex (new Vector2f(742, 139), Color.Black), new Vertex (new Vector2f(793, 141), Color.Black),
			new Vertex (new Vector2f(887, 414), Color.Black), new Vertex (new Vector2f(589, 635), Color.Black)
		};

		public List<Vertex[]> innerTrackLines;

		public List<Vertex[]> allTrackLines;

		public GameManager (WindowManager window) {
			wm = window;
			player = new Car(this);
			GenerateTrack();
		}

		public void Update () {
			player.Update();

			DrawTrack();
		}

		public void GenerateTrack () {
			outerTrackLines = new List<Vertex[]>();
			innerTrackLines = new List<Vertex[]>();
			allTrackLines = new List<Vertex[]>();

			//innerTrack = new ConvexShape((uint)outerTrackVertices.Count);

			for (int i = 0; i < outerTrackVertices.Count; i++) {
				//outer 
				if (i == outerTrackVertices.Count - 1) {
					var temp = new List<Vertex>() { outerTrackVertices[i], outerTrackVertices[0] };
					outerTrackLines.Add(temp.ToArray());
				} else {
					var temp = new List<Vertex>() { outerTrackVertices[i], outerTrackVertices[i + 1] };
					outerTrackLines.Add(temp.ToArray());
				}
			}

			for (int i = 0; i < innerTrackVertices.Count; i++) {
				//inner 
				if (i == innerTrackVertices.Count - 1) {
					var temp = new List<Vertex>() { innerTrackVertices[i], innerTrackVertices[0] };
					innerTrackLines.Add(temp.ToArray());
				} else {
					var temp = new List<Vertex>() { innerTrackVertices[i], innerTrackVertices[i + 1] };
					innerTrackLines.Add(temp.ToArray());
				}
			}

			allTrackLines.AddRange (outerTrackLines);
			allTrackLines.AddRange(innerTrackLines);
		}

		public void DrawTrack () {
			foreach (Vertex[] line in allTrackLines) {
				wm.window.Draw(line, PrimitiveType.Lines, RenderStates.Default);

				
			}
		}
	}
}
